package com.zuitt.example;

//interface implementations
public class Person implements Actions, Greetings {
    public void sleep() {
        System.out.println("Zzzzz...");
    }

    public void run(){
        System.out.println("Running.");
    }

    public void morningGreet(){
        System.out.println("Good morning!");
    }
    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }
}
