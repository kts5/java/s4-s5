package com.zuitt.example;

public class StaticPoly {
    public int addition(int a, int b){
        return a + b;
    }

    public int addition(int a, int b, int c){
        return a + b + c;
    }
}
